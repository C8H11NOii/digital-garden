---
title: "Software Development"
createdDate: 2023-03-13
bookFlatSection: true
bookCollapseSection: false
weight: 5
---

I spend a lot of my free time learning, tinkering and creating. Many of these projects involve some aspect of computing. I also work as an Agile Delivery Manager in the Public Sector.

On this branch of the garden I talk all things software; from Agile and Lean Practices, to Operating Systems and distributed compute.
<!--more-->

{{< columns >}}

{{< get-summary "software-development/coding" >}}

<--->

{{< get-summary "software-development/operating-systems" >}}

{{< /columns >}}

{{< get-summary "software-development/agile" >}}
