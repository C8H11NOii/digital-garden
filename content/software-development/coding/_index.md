---
title: "Coding"
createdDate: 2023-03-13
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 10
---

This branch is dedicated to writing code, detailing the tooling, languages and best practice for writing code.

I mostly speak python and I have opinions on git. I use Vim as by default text editor.

<!--more-->

{{< section >}}
