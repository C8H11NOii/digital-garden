---
title: Rebase All Local Branches
createdDate: 2023-03-24T09:48:14Z
bookHidden: true
draft: false
weight: 5
---

This is a little hack-y but works. There's probably a more robust solution.

```sh
git branch | cut -c 3- | for branch in $(cat); do git rebase main $branch; done
```

<!--more-->
