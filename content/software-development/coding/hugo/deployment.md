---
title: Deployment
createdDate: 2023-03-23T17:07:59Z
bookHidden: true
draft: false
weight: 5
---

There are several kinds of deployment in Hugo. Hugo ships with its own development server which is not intended for production environments. It is possible to provide credentials in the Hugo config file that mean you can deploy directly to certain platform providers. However, all this does it automate several of the steps that we're going to go into here.

**TL;DR:** you're going to want to template out your Hugo site using the `Hugo` command from the root of your Hugo project and then transfer the generated `public/` directory onto your sites host. How you do that is the interesting part.

<!--more-->

## Gitlab Pages

Gitlab pages give you a great and flexible deployment option that can trigger easily from CI builds. This also means that you can run any static analysis or tests on your site before you deploy to production and break everything: bonus!

Gitlab has a Hugo image already in their CI-CD image registry - which also takes a lot of the pressure off creating and registering your own container image.

{{< hint info >}}
I have struggled to succeed with fixed Hugo image tags from the Gitlab registry. The `:latest` tag works, but be mindful that Hugo is in active development and whilst breaking changes in how Hugo works are rare, they do happen. Be especially mindful when using a theme that has fallen out of support or is only supported by an individual. Resolving underlying issues can take time to resolve.
{{< /hint >}}

### The .gitlab-ci.yml file

In your `.gitlab-ci` file you need to 'build' your site by running the Hugo templating engine, generating the `./public` directory. You then need to declare this directory as a build artefact, which is what pages ultimately makes available as your site.

It is worth checking the Gitlab CI documentation at this point, and if you find that this config isn't quite what you need then you may find more answers in the main docs.

```yml
variables:
  GIT_SUBMODULE_STRATEGY: recursive

stages:
- build
- deploy

# seperating out the build stage ensures that your site will build
# This should catch build failures before you merge to main

build:
  image: registry.gitlab.com/pages/hugo/hugo_extended:latest
  stage: build
  artifacts:
    paths:
    - ./public
  script:
    - hugo

pages:
  stage: deploy
  dependencies:
  - build
  script:
  - echo "Building site" # needed because pages needs a script
  artifacts:
    paths:
    - ./public
  rules: # makes this stage conditional on changes to main
    if: $CU_COMMIT_BRANCH == $CI_DEFAULT_BRACH
```

### Pages URL

By default Gitlab builds to a URL that looks something like `<group-name>.gitlab.io/<project>`. Whilst functional and potentially useful in some instances, if you want to deploy to a subdomain of your domain, you might want to explore some *hackery* in the DNS settings for your DNS provider and the Gitlab settings page.
