---
title: Templates (Archetypes)
createdDate: 2023-03-23T17:07:35Z
bookHidden: true
draft: false
weight: 5
---

Archetypes are template content files that contain pre-configured content that can be used when calling `hugo new`.

<!--more-->
