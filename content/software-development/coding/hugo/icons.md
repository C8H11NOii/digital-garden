---
title: Icons
createdDate: 2023-04-09T22:17:31Z
bookHidden: true
draft: false
weight: 5
---

It is possible to retrieve `svg` icons from an icon provider such as `font-awesome`, by embedding `javascript` into your site that makes a call out to font-awesome to retrieve the icon. Given the relatively small size of an 'icon' this is probably excessive for most static site generation.

Instead, you should take a few steps to embed icons within the directory structure of your site. By
minimally increasing the storage footprint of a site, you may be able to significantly improve the performance of your site and cut out unnecessary network calls.

<!--more-->

## The Traditional way

Sites like `font-awesome` traditionally point you towards defining a script in the header element of the site such as:

```html
<script defer src="/static/fontawesome/fontawesome-all.js"></script>
```

You can then retrieve an icon using something like:

```html
<i class="fab fa-gitlab"></i>
```

## What we're aiming for

Using what we know of shortcodes and partial templates, we want to get to a point where we can make simple references within our site content so that Hugo will know to render a specific `inline` icon, with something like `{{ partial "icon.html" "git-merge" }}` or {{% escaped-code code="{{% icon git-merge %}}" %}}

## Getting started

Create a directory for your svg files (you could nest an icon directory in here). I went with the `./assets` directory, but you could go with `./layouts/partials` depending on how you feel.

```
├── assets
│   ├── svg
│   │   ├── your-custom-icon.svg
```

Once you have your destination folder, you're going to want to download, create or otherwise locate the icons that you want addition to your site.

If you are using font-awesome icons, their free icons have been open sourced on github at [FortAwesome/Font-Awesome](https://github.com/FortAwesome/Font-Awesome). Their release branches are relatively stable and in release `6.x` icons are located in the `svgs` directory.

If you want to live a little, and don't want to leave the comfort of your command line, you're going to need a curl command.

```sh
curl -0 "https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/brands/gitlab.svg" > "layouts/svg/gitlab.svg"
```

This will export the `svgs/brands/gitlab.svg` file on the `6.x` release branch to a file called `gitlab.svg` in the directory you created earlier.

Its best to have a look in the github repository to make sure that the file that you are downloading is there and is called the thing you think it is called.

## Now on to the fun stuff

Create a `icon.html` file in the `./layouts/partials/` directory. This is where you will create the partial that can be called from within `.html` files.

```html
<span class="inline-svg" >
{{- $fname:=print "assets/svg/icons/" . ".svg" -}}
{{- $path:="<path" -}}
{{- $fill:="<path fill=\"currentColor\"" -}}
{{ replace (readFile $fname) $path $fill | safeHTML }}
</span>
```

Next, create a similar looking file in the `./layouts/shortcodes/` directory and call it `icon.html` for arguments sake.

```html
<span class="inline-svg" >
{{- $fname:=print "assets/svg/icons/" ( .Get 0 ) ".svg" -}}
{{- $path:="<path" -}}
{{- $fill:="<path fill=\"currentColor\"" -}}
{{ replace (readFile $fname) $path $fill | safeHTML }}
</span>
```

Try to avoid a sneaking EOF new line - it can do weird things to the layout

## Wrap up

With that, you should be set!, you should now be able to reference `svg` files in the content of your site. You will have also avoided any pesky networked dependencies for an icon of a few hundred bytes.

## References

- [Font Awesome](https://fontawesome.com/)
- [Using font-awesome icons in hugo](https://www.client9.com/using-font-awesome-icons-in-hugo/)
- [Font Awesome Github](https://github.com/FortAwesome/Font-Awesome)
