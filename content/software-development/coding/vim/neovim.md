---
title: NeoVim
createdDate: 2023-03-14
bookHidden: true
---

I am interested in the [Lua](https://www.lua.org/about.html) based [neo-vim](https://github.com/neovim/neovim) and am interested in its claims of performance improvements and greater flexibility.

I have experimented a little with the editor and identified some interesting differences that I do not fully understand yet. One notable difference is how you interact with the built in terminal emulator.
