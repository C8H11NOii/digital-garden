---
title: "Plug-ins"
createdDate: 2023-03-14
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

Whilst it is possible to manually add new plug-ins by adding them, to the `.vim` directory in the user's `$HOME` directory. Your quality of life will be vastly improved if you use a pre-built package manager. I use [vim-plug](https://github.com/junegunn/vim-plug) by junegunn which allows you to manage plug-ins directly from your `.vimrc`

<!--more-->

## Getting started with plug-ins

On clean installs of [Vim]({{< ref "/software-development/coding/vim" >}}), where vim-plug isn't installed, you can either follow the install instructions in the README of the project, or you can add the following to download the plug-in manager on the fly.

```vim-script
    if empty(glob('~/.vim/autoload/plug.vim'))
        silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    endif
```

If you try and run Vim without installing this plug in manager, and you reference it within your `.vimrc` vim will error on each start up.

### Auto loading plug-ins

You can also configure vim to run `PlugInstall` on opening vim to install any uninstalled vim plug-ins.

```vim-script
    " Run PlugInstall if there are missing plug-ins
    autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
        \| PlugInstall --sync | source $MYVIMRC
    \| endif
```

{{< section >}}
