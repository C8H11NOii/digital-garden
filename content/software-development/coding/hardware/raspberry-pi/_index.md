---
title: "Raspberry Pi"
createdDate: 2023-03-14
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

The Raspberry Pi is a type of Single Board Computer that has built a reputation for stability and functionality that far outstrips many of its competitors. The Raspberry Pi 4 (Model B) ships with a 1.5 GHz quad-core ARM processor that gives it sufficient compute to take on elements of desktop processing.

<!--more-->

{{< section >}}
