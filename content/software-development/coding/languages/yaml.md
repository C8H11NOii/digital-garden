---
title: Yaml
createdDate: 2023-07-06T04:22:27Z
bookHidden: true
draft: false
weight: 5
---

**YAML** (rhymes with camel) is a 'human readable' structured data. YAML can exist either in a stand alone file, or be parsed within another file. YAML is an acronym for "*YAML Ain’t Markup Language*"

Structure is created within YAML through use of specific characters, with nested elements implied through indentation.

<!--more-->

There are many commonalities in concepts between YAML and JSON standards. Although they have been developed independently, the YAML language development team have worked hard to ensure the YAML remains a strict 'superset' of the JSON standard.

## History

YAML 1.0 was released in 2004, with updates in 2005 (1.1) and 2009 (1.2). Since 2009, YAML adoption has increased, and since 2020 there has been active work and discussion in how to improve and refine the standard.

### YAML or YML

`.yaml` and `.yml` file extensions will both be interpreted as YAML and follow the same syntax as defined on the [yaml.org](https://yaml.org) website.

In older versions of [Windows]({{< relref "../../operating-systems/windows/" >}}) file extensions were restricted to three characters, hence `.yml`. This is no logner the case and the community has largely moved towards `.yaml` as the preferred file extension.

## Data Types

YAML makes concerted effort to map values to specific data types. Typing is dynamically inferred from the value however, explicit types can be set using a double exclamation mark `!!`. Key values without explicit typing are referred to as 'non-specific tags' within the YAML documentation (with typed tags `string: !!str 'This is a string'`, being known as 'specific tags').

There is no need to explicitly type YAML entries, however, it is worth referring to any intermediary Language documentation to confirm their support for specific formats. YAML groups their mechanisms for resolving non-specific tags, into schemas.

The Core Language schema, resolve the following examples as follows.

```yaml
A null: null
Also a null: # Empty
Not a null: ""
Booleans: [ true, True, false, FALSE ]
Integers: [ 0, 0o7, 0x3A, -19 ]
Floats: [
  0., -0.0, .5, +12e03, -2E+05 ]
Also floats: [
  .inf, -.Inf, +.INF, .NAN ]
```

The YAML documentation on [resolving data types](https://yaml.org/spec/1.2.2/#resolved-tags) goes into a lot more depth.

## Collections

All YAML entries are nodes within the data structure, these nodes can contain collections of nodes which can be scoped by indentation of the block.

Collections fall into one of three main kinds: Sequence, Mapping, Scalar. It is possible to nest collections within another collection.

{{< details "Examples" >}}

```yaml
# sequence of scalars
rivers:
- Severn
- Thames
- Trent

# sequence of mappings
rivers:
- name: Severn
  length-km: 354
  mean-flow: 107.4
- name: Thames
  length-km: 346
  mean-flow: 65.4
- name: Trent
  length-km: 297
  mean-flow: 89.0

# sequence of sequences
rivers:
- [name  , length-km, mean-flow ]
- [Severn,       354,     107.4 ]
- [Thames,       346,      65.4 ]
- [Trent ,       297,      89.0 ]
```
{{< /details >}}

### Sequence

All Sequences use a dash and a space (`- `)to denote each entry within the sequence. Sequences can be nested within a sequence by using `[]` square brackets.

Sequences are ordered, and can contain zero or more nodes. There are no restrictions on the same node appearing multiple nodes.

### Mapping

A mapping contains a nested series of `key: value` pairs. The content of a mapping is unordered, but each key must be unique.

It is possible to nest further collections within a mapping.

### Scalars

Are a catch-all kind for any node without a clear collection or data type and presented as a series of Unicode characters. Scalars can be written as 'Blocks' or as 'Flows' and can be plain, single quoted or double quoted.

Block scalars are indented, but use a specific character after the `:` to indicate how newlines within the block should be handled. Literal block scalars can use pipe `|` to retain new line characters. Folded block scalars can use `>` to replace new line characters with spaces, unless followed by an empty or more-indented line.

Flow scalars can be represented over multiple lines as well, and can either start on the same line as the `key:` or on the next line. How they manage newlines, escaped characters and specific characters depends on whether they are 'plain', 'single-quoted' or 'double-quoted'. [Yaml-multiline](https://yaml-multiline.info/) has some really clear examples of how the various Scalar features interact and example use cases.

## References

- [yaml.org](https://yaml.org/)
- [YAML standard v1.2.2](https://yaml.org/spec/1.2.2/)
- [YAML multi-line](https://yaml-multiline.info/)
