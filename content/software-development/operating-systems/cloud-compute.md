---
title: "Cloud Compute"
createdDate: 2023-03-14T14:54:33Z
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

Managing remote servers and devices is an important part of application development. Whether the remote service is provided on your own hardware or provided as part of a Virtual Private Server, the step up considerations should be similar:

- How will you access your server
- How will you secure your server
- What monitoring or alerting do you require

<!--more-->

## Change the Root password

First login as root to the device, your prompt should end with a `#` symbol if you are the root user.

From the command prompt type the `passwd` command to change the root password. To change the root password as a non root user, type `sudo passwd root`

## Creating a non-root user

To create a new user, use the `useradd {username}` command (replacing `{username}` with the name of the user that you want to add.

Once created, you will want to give them a default password which you can do with running the `passwd {username}` command.

If you want this user to have a directory in `/home/`, run `useradd -m {username} -s /bin/bash`.

Debian systems also ship with an `adduser` command that has an interactive prompt that will result in similar outputs.

### Optional `sudo` powers

If you want to give sudo powers to your new user you can run `echo '{username} ALL=(ALL) ALL' >> /etc/sudoers`

## Disable root login

The `ssh` config file is located in `/etc/ssh/sshd_config`. You need to find the line that says `PermitRootLogin yes` and change it to `no`.

You will need to use an editor, if [vim]({{ relref "/software-development/coding/vim/ }}) isn't installed on your system, do every one a favour and install it whilst your here.

Now you need to restart the ssh service, run `/etc/init.d/ssh restart` (note on some systems it might be `/etc/init.d/sshd`.

Before closing your connection, make sure that you can still connect to your remote server by opening a new terminal window and try to connect to the account you configured earlier. Whilst your at it, why not try and connect to the root user...

## Change the default ssh port

There are many reasons why you should not use the default `port 22` to connect to your devices, especially if this is a remote device exposed to the internet.

Broadly speaking it doesn't matter what port you choose, but you should choose a number between **1024** and **65536** to avoid conflicting with other running services on the remote machine. You can check for conflicts against a [list of TCP and UDP port numbers](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) and their usage. Anything in the range of 5 digits should be ok.

Having identified the port number that you want to use, locate the line that specifies `Port 22` in `/etc/ssh/sshd_config` and change it to your port number of choice.

You will need to restart the ssh service again, `/etc/init.d/ssh restart`.

Before leaving your session you will need to make sure that you can still connect to the remote machine - you may need to edit firewall sessions on your Machine or with your provider.
