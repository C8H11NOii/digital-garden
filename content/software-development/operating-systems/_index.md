---
title: "Operating Systems"
createdDate: 2023-03-17
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 15
---

Operating Systems manage the computer hardware and software resources on a machine, providing common services for computer programs.

I am a long time MacOS user who is increasingly leaning towards Debian and other flavours of Linux.

<!--more-->

{{< section >}}
