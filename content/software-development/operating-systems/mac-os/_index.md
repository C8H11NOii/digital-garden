---
title: "macOS"
createdDate: 2023-03-21
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

macOS is a proprietary operating system developed by Apple Inc. for their line of Macintosh computers. The core Unix operating system of macOS is called Darwin, providing the underlying functionality of the operating system, such as hardware management, memory allocation, and process scheduling. Darwin shares many of the same components and features of its cousin the Linux kernel, but with modifications and 'enhancements' made by Apple.

One of the key differences between Darwin and its Linux counterparts is that it does not ship with a built in package manager. To install packages and tools from the wider Unix community, you need to install a package manager such as [Homebrew](https://brew.sh/)

<!--more-->

{{< section >}}
