---
title: Dying
createdDate: 2023-03-13
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 30
---

Dying projects are projects that have fallen out of support and are not actively maintained. They may have been abandoned or run their course. In either sense these are projects that, if left, will die.

If there is sufficient community support to revive a dying project, I am happy to consider how the project is maintained.

<!--more-->

{{< section >}}
