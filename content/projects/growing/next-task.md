---
title: "Next Task"
createdDate: 2023-03-12
---

Next Task is a task management project. Taking inspiration from [TaskWarrior](https://taskwarrior.org/), this project started out as a simplified command line interface writing tasks to a file in the `$HOME` directory.

Since then, the project has morphed and changed as my understanding of coding practices, databases and distributed compute has grown. As such it is broadly an experimental scratch-pad framed around a simple utility.
<!--more-->

## Philosophy

I am a strong advocate for good 'old-fashioned' paper task list. A well organised, actionable task list is a key tenant of being a productive person.

## Requirements

Requirements are prioritised using the [MoSCoW]() method.

#### Must Haves

- The interface **must** be simple
- The tasks **must** be prioritised
- The performance of the interface **must** be optimised for performance
- The task list **must** be independent of other users of the task list
- The user **must** be able to request the next priority task
- The user **must** be able to 'skip'
- The user **must** be able to 'complete' a task

#### Should Haves

- The user **should** be able to add categories to a task to group similar tasks
- The user **should** be able to request tasks based on a category

#### Could Haves

- Completed tasks **could** be available retrospectively

## Project

[Repository](https://gitlab.com/makejames/next-task)

### Project History

### Technology

- Python 3.11
- FastApi
- PostgreSQL
