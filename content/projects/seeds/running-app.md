---
title: Fitness Tracking
createdDate: 2023-04-11T20:01:51Z
bookHidden: true
draft: false
weight: 5
---

The fitness tracking tech industry is vast and prolific. This application would seek to bring significant portions of that reach under my ownership.

- I want to own my fitness and location data.
- I want a solution that will provide analytics information on my performance over time
- I want to record geo-location data 'on the go'

<!--more-->

## Context

The fitness tracking tech industry is vast and prolific. There are many pre-existing solutions in wearables, gps tracking and visualisation tools. Most of these come at either significant upfront cost to the user or operate a freemium offer that hides significant portions of the application behind a paywall. Moving beyond monetary cost, the integration of social, health and location data means users are willingly sharing a significant amount of data about themselves freely.

My main concerns are:

- Privacy considerations of location data
- Links to large corporate advertising companies, particularly through social media

