---
title: Dormant
createdDate: 2023-03-11
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 15
---


Dormant projects are projects that have grown and are not actively growing. These are projects that I consider 'complete', they may have entered my tool chain and be actively used, but I am not actively contributing new features to the project.

These projects will be maintained. For code projects, this means patching functional defects, resolving security vulnerabilities and ensuring that libraries and supporting packages remain compatible with supported versions.

<!--more-->

Projects that are not 'complete', but are not being actively grown should move back to being seeds rather than progressed to a dormant state. This is because incomplete projects might need to be re-designed should they sprout again.

{{< section >}}
