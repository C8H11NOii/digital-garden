---
menu:
  before:
    name: about
    weight: 1
title: About
createdDate: 2023-03-09
bookHidden: true
---

I am a musician, coder, and scrum master and I like to make things.

Between tinkering, collaboration, reading, this Digital Garden is my own voyage of discovery.
