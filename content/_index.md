---
title: Introduction
createdDate: 2023-03-09
type: docs
---

This is a place of personal exploration, documenting my learning across the range of my interests.

Unlike traditional blogging sites, the content here is grouped by themes and guided by my interests and my learning.

{{< columns >}}

{{< get-summary "software-development" >}}

<--->

{{< get-summary "projects" >}}

{{< /columns >}}

## Visiting the Garden

The focus here is more on the process of learning and discovery than creating a polished product. So, some posts may be a little rough round the edges, some will just be seeded thoughts that need to be nurtured.

This means that some pages may be more complete that others.

If you want to suggest an edit or an enhancement the source of this project is available on [gitlab](https://gitlab.com/makejames/digital-garden), feel free to raise an issue and if you feel up to it, create a merge request with the changes.
